﻿using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField] private int m_MoveSpeed;
    [SerializeField] private bool m_IsPlayer1;

    private int m_CurrentSpeed;
    private Vector3 m_InputValues;
    private Rigidbody m_RB;

    void Start()
    {
        m_RB = GetComponent<Rigidbody>();
    }

    void Update()
    {
        if (m_IsPlayer1)
        {
            if (Input.GetAxis("Horizontal Player 1") > 0.0f)
            {
                m_CurrentSpeed = m_MoveSpeed;
            }
            if (Input.GetAxis("Horizontal Player 1") < 0.0f)
            {
                m_CurrentSpeed = -m_MoveSpeed;
            }
            if (Input.GetAxis("Horizontal Player 1") == 0.0f)
            {
                m_CurrentSpeed = 0;
            }
        }
        else
        {
            if (Input.GetAxis("Horizontal Player 2") > 0.0f)
            {
                m_CurrentSpeed = m_MoveSpeed;
            }
            if (Input.GetAxis("Horizontal Player 2") < 0.0f)
            {
                m_CurrentSpeed = -m_MoveSpeed;
            }
            if (Input.GetAxis("Horizontal Player 2") == 0.0f)
            {
                m_CurrentSpeed = 0;
            }
        }
    }

    void FixedUpdate()
    {
        m_InputValues.x = m_CurrentSpeed * Time.fixedDeltaTime;
        
        if (GlobalManager.GameIsRunning)
            m_RB.MovePosition(transform.position + m_InputValues);
    }
}
