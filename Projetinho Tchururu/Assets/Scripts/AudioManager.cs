﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class AudioManager : MonoBehaviour
{
    AudioManager Instance;

    [SerializeField] Toggle m_Toggle;
    [SerializeField] AudioSource m_BackgroundMusic;
    [SerializeField] AudioSource m_WhistleSFX;
    [SerializeField] AudioSource m_CrowdSFX;
    [SerializeField] AudioSource m_EndMatchSFX;

    void Awake()
    {
        if (Instance == null) Instance = this;
        else Destroy(gameObject);
    }

    void OnEnable()
    {
        Ball.OnGoalAudio += PlayGoalAudios;
        GlobalManager.OnStartMatch += PlayBackgroundMusic;
        GlobalManager.OnEndMatch += PlayEndMatchSFX;
    }

    void OnDisable()
    {
        Ball.OnGoalAudio -= PlayGoalAudios;
        GlobalManager.OnStartMatch -= PlayBackgroundMusic;
        GlobalManager.OnEndMatch -= PlayEndMatchSFX;
    }

    public void ControlBackgroundMusic()
    {
        m_BackgroundMusic.mute = !m_Toggle.isOn;
    }

    void PlayGoalAudios()
    {
        if (!m_WhistleSFX.isPlaying)
        {
            m_WhistleSFX.Play();
        }
        else
        {
            m_WhistleSFX.Stop();
            m_WhistleSFX.Play();
        }

        if (!m_CrowdSFX.isPlaying)
        {
            m_CrowdSFX.Play();
        }
        else
        {
            m_CrowdSFX.Stop();
            m_CrowdSFX.Play();
        }

        StartCoroutine(DecreaseBackgroundMusicOnGoal());
    }

    void PlayBackgroundMusic()
    {
        if (m_EndMatchSFX.isPlaying)
            m_EndMatchSFX.Stop();
        if (!m_BackgroundMusic.isPlaying)
            m_BackgroundMusic.Play();
    }

    void PlayEndMatchSFX()
    {
        if (m_BackgroundMusic.isPlaying)
            m_BackgroundMusic.Stop();
        if (!m_EndMatchSFX.isPlaying)
            m_EndMatchSFX.Play();
    }

    IEnumerator DecreaseBackgroundMusicOnGoal()
    {
        m_BackgroundMusic.volume = 0.05f;
        yield return new WaitForSeconds(2.0f);
        m_BackgroundMusic.volume = 0.2f;
    }
}
