﻿using System.Collections;
using UnityEngine;

public class Ball : MonoBehaviour
{
    #region SerializeField variables
    [SerializeField] private int m_InitialSpeedX;
    [SerializeField] private int m_InitialSpeedZ;
    #endregion

    #region Private variables
    private int m_CurrentSpeedX;
    private int m_CurrentSpeedZ;
    private Rigidbody m_RB;
    private Vector3 m_CurrentMovementSpeed;
    private Vector3 m_InitialPosition;
    #endregion

    #region Events
    public delegate void Goal(string scorer);
    public static event Goal OnGoal;

    public delegate void GoalAudio();
    public static event GoalAudio OnGoalAudio;
    #endregion

    void Start()
    {
        m_InitialPosition = transform.position;
        m_RB = GetComponent<Rigidbody>();
        RandomizeDirection();
    }

    void FixedUpdate()
    {
        if (GlobalManager.GameIsRunning)
        {
            m_CurrentMovementSpeed.x = m_CurrentSpeedX * Time.fixedDeltaTime;
            m_CurrentMovementSpeed.z = m_CurrentSpeedZ * Time.fixedDeltaTime;
            m_RB.MovePosition(transform.position + m_CurrentMovementSpeed);
        }
    }

    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Wall"))
        {
            m_CurrentSpeedX *= -1;
        }

        if (other.gameObject.CompareTag("Player"))
        {
            m_CurrentSpeedZ *= -1;
        }

        if (other.gameObject.CompareTag("Post"))
        {
            m_CurrentSpeedX *= -1;
            m_CurrentSpeedZ *= -1;
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Goal Blue"))
        {
            OnGoal?.Invoke("blue");
            OnGoalAudio?.Invoke();
            StartCoroutine(CallResetBall());
        }
        if (other.gameObject.CompareTag("Goal Red"))
        {
            OnGoal?.Invoke("red");
            OnGoalAudio?.Invoke();
            StartCoroutine(CallResetBall());
        }
    }

    public void ResetBall()
    {
        transform.position = m_InitialPosition;
        RandomizeDirection();
    }

    void RandomizeDirection()
    {
        m_CurrentSpeedX = Random.value > 0.5 ? m_InitialSpeedX : -m_InitialSpeedX;
        m_CurrentSpeedZ = Random.value > 0.5 ? m_InitialSpeedZ : -m_InitialSpeedZ;
    }

    IEnumerator CallResetBall()
    {
        yield return new WaitForSeconds(2.0f);
        ResetBall();
    }
}
