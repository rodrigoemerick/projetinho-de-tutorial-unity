﻿using System.Collections;
using UnityEngine;
using TMPro;

public class GlobalManager : MonoBehaviour
{
    public static bool GameIsRunning;

    private GlobalManager Instance;

    [SerializeField] GameObject m_StartScreen;
    [SerializeField] TMP_Text m_PlayerOneScoreText; 
    [SerializeField] TMP_Text m_PlayerTwoScoreText;
    [SerializeField] TMP_Text m_GameTimerText;
    [SerializeField] float m_GameTime;
    [SerializeField] GameObject m_EndScreen;
    [SerializeField] TMP_Text m_EndMatchMessage;
    [SerializeField] Ball m_Ball;

    private int m_ScorePlayerOne;
    private int m_ScorePlayerTwo;
    private float m_CurrentGameTime;

    public delegate void StartOrEndMatch();
    public static event StartOrEndMatch OnStartMatch;
    public static event StartOrEndMatch OnEndMatch;

    void Awake()
    {
        if (Instance == null) Instance = this;
        else Destroy(gameObject);
    }

    void OnEnable()
    {
        Ball.OnGoal += GiveGoal; 
    }

    void OnDisable()
    {
        Ball.OnGoal -= GiveGoal;
    }

    void Start()
    {
        m_CurrentGameTime = m_GameTime;
    }

    void Update()
    {
        if (m_CurrentGameTime > 0.0f && GameIsRunning)
        {
            m_CurrentGameTime -= 1.0f * Time.deltaTime;
            m_GameTimerText.text = Mathf.RoundToInt(m_CurrentGameTime).ToString();
        }
    }

    public void StartGame()
    {
        GameIsRunning = true;
        m_StartScreen.SetActive(false);
        m_Ball.ResetBall();
        m_ScorePlayerOne = 0;
        m_ScorePlayerTwo = 0;
        UpdateScores();
        m_CurrentGameTime = m_GameTime;
        StartCoroutine(WaitForMatchEnd());
        OnStartMatch?.Invoke();
    } 

    public void RestartGame()
    {
        GameIsRunning = true;
        m_EndScreen.SetActive(false);
        m_Ball.ResetBall();
        m_ScorePlayerOne = 0;
        m_ScorePlayerTwo = 0;
        UpdateScores();
        m_CurrentGameTime = m_GameTime;
        StartCoroutine(WaitForMatchEnd());
        OnStartMatch?.Invoke();
    }

    void GiveGoal(string scorer)
    {
        switch (scorer)
        {
            case "blue":
                ++m_ScorePlayerTwo;
                break;
            case "red":
                ++m_ScorePlayerOne;
                break;
        }

        UpdateScores();
    }

    void UpdateScores()
    {
        m_PlayerOneScoreText.text = $"Player 1: {m_ScorePlayerOne}";
        m_PlayerTwoScoreText.text = $"Player 2: {m_ScorePlayerTwo}";
    }

    void ShowEndScreen()
    {
        OnEndMatch?.Invoke();

        if (m_ScorePlayerOne > m_ScorePlayerTwo)
            m_EndMatchMessage.text = "Player 1 Won!";
        else if (m_ScorePlayerTwo > m_ScorePlayerOne)
            m_EndMatchMessage.text = "Player 2 Won!";
        else
            m_EndMatchMessage.text = "Tie!";

        GameIsRunning = false;
        m_EndScreen.SetActive(true);
    }

    public void ReturnToMainMenu()
    {
        m_EndScreen.SetActive(false);
        m_StartScreen.SetActive(true);
    }

    IEnumerator WaitForMatchEnd()
    {
        yield return new WaitUntil(() => m_CurrentGameTime <= 0.0f);
        ShowEndScreen();
    }
}
